const fotoDiri = document.querySelector('img');
const tombolUbahFoto = document.querySelector('#tombol-ubah-foto');
const inputFoto = document.querySelector('#input-foto');
const inputNama = document.querySelector('#input-nama');
const tampilanNama = document.querySelector('#tampilan-nama');

// Menambahkan event listener ke tombol "Ubah Foto"
tombolUbahFoto.addEventListener('click', function() {
  inputFoto.click();  // Simulasikan klik ke input "Pilih Foto"
});

// Menambahkan event listener ke input "Pilih Foto"
inputFoto.addEventListener('change', function() {
  const fileFoto = inputFoto.files[0];
  const urlFoto = URL.createObjectURL(fileFoto);
  fotoDiri.src = urlFoto;
});

// Menambahkan event listener ke input "Nama Lengkap"
inputNama.addEventListener('input', function() {
  tampilanNama.textContent = inputNama.value;
});