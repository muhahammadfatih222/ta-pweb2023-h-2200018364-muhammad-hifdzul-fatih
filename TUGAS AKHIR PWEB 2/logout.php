<?php
session_start(); // Mulai session (pastikan ini diletakkan di awal file)

// Hapus semua data session
session_unset();

// Hancurkan session
session_destroy();

// Redirect ke halaman login atau halaman lain yang diinginkan setelah log out
header('Location: loginpage.php');
exit();
?>
