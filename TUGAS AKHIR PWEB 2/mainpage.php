<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">
  <title>TUGAS AKHIR PWEB 2023</title>
  <style>
    body {
      position: relative;
      margin: 0;
      padding: 0;
      height: 100%;
      background-color: rgba(93, 119, 255, 1);
      font-family: "Roboto", sans-serif;
    }

    html {
      scroll-behavior: smooth;
    }

    .container {
      display: flex;
      width: 100%;
    }

    .wrapper {
      display: flex;
      position: fixed;
      justify-content: space-between;
      align-items: center;
      font-size: 20px;
      padding: 5px;
      width: 100%;
      height: 80px;
      background-color: black;
      box-shadow: 0 7px 15px 0 rgba(0, 0, 0, 0.5);
    }

    .brand {
      display: flex;
      flex-direction: row;
      align-items: center;
      font-size: 1.5em;
      padding: 15px;
      text-transform: capitalize;
    }

    .firstname {
      color: white;
      font-weight: 700;
    }

    .lastname {
      color: rgb(213, 177, 33);
      font-weight: 400;
      padding-left: 4px;
    }

    .navigation {
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .navigation>li {
      list-style-type: none;
      padding: 15px;
    }

    .navigation>li>a {
      color: white;
      font-size: 20px;
      text-decoration: none;
      text-transform: capitalize;
    }

    .navigation>li>a:hover {
      color: rgb(242, 255, 0);
      transition: all .3s ease-in-out;
    }

    .active {
      color: white;
      padding: 15px;
    }

    .active:hover {
      color: white;
    }

    .pesan {
      display: flex;
      justify-content: center;
      align-items: center;
      min-height: 100vh;
      background-color: rgba(93, 119, 255, 1);
    }

    form {
      display: flex;
      flex-direction: column;
      align-items: center;
      padding: 20px;
      border: 1px solid #ccc;
      border-radius: 10px;
      background-color: #fff;
      max-width: 600px;
    }

    .tombolpesan {
      background-color: rgb(213, 177, 33);
      color: black;
      padding: 10px 20px;
      border: none;
      border-radius: 4px;
      cursor: pointer;
      float: right;
      margin-top: 20px;
    }

    h1 {
      text-align: center;
    }
    h2 {
      text-align: center;
    }
    /* Responsive Styles */
    @media (max-width: 768px) {
      .brand {
        font-size: 1.2em;
      }

      .navigation {
        flex-direction: column;
      }

      .navigation>li {
        padding: 10px 0;
      }

      .pesan {
        align-items: flex-start;
        padding: 20px;
      }

      form {
        width: 100%;
      }
    }
    footer {
  background-color: #333;
  color: #fff;
  padding: 20px 0;
  width: 100%;
  text-align: center;
}

footer p {
  margin: 0;
  font-size: 14px;
}
  </style>
</head>

<body>
  <header>
    <div class="container">
      <nav class="wrapper">
        <div class="brand">
          <div class="firstname">TICKET</div>
          <div class="lastname">thing.</div>
        </div>
        <ul class="navigation">
          <li><a href="mainpage.php">Home</a></li>
          <li><a href="aboutpage.php" class="active">about me</a></li>
          <li><a href="contactpage.php">contact me</a></li>
          <li><a href="logout.php">log out</a></li>
        </ul>
      </nav>
    </div>
  </header>
  <div class="pesan">
    <form action="pesanpage.php" method="post" name="input">
      <h1>WELCOME TO TICKETHING</h1>
      <h2>Website ini adalah website personal saya dan saya juga menyediakan tiket nonton konser musik untuk anda</h2>
      <div class="tombolpesan">
        <input type="submit" name="Login" value="PESAN TIKET" />
      </div>
    </form>
  </div>
  <footer>
  <div class="foot">
    <p>&copy; 2200018364-Muhammad Hifdzul Fatih. 2023 TICKETTHING. All rights reserved.</p>
  </div>
</footer>
</body>

</html>
