<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>TUGAS AKHIR PWEB 2023</title>
    <style>
        * {
    margin: 0;
    padding: 0;
}

body {
    position: relative;
    margin: 0;
    padding: 0;
    height: 100%;
    background-color: rgba(93, 119, 255, 1);
    font-family: "Roboto", sans-serif;
     
}

html {
    scroll-behavior: smooth;
}
.container {
    display: flex;
    width: 100%;
}

.wrapper {
    display: flex;  
    position: fixed;
    justify-content: space-between;
    align-items: center;
    font-size: 20px;    
    padding: 5px;
    width: 100%;
    height: 80px;   
    background-color:black;
    box-shadow: 0 7px 15px 0 rgba(0, 0, 0, 0.5);
}

.brand {
    display: flex;
    flex-direction: row;
    align-items: center;
    font-size: 1.5em;
    padding: 15px;
    text-transform: capitalize;
}

.firstname {
    color: white;
    font-weight: 700;
}

.lastname {
    color: rgb(213, 177, 33);
    font-weight: 400;
    padding-left: 4px;
}

.navigation {
    display: flex;
    justify-content: center;
    align-items: center;
}

.navigation>li {
    list-style-type: none;
    padding: 15px;
}

.navigation > li > a {
    color: white;
    font-size: 20px;
    text-decoration: none; 
    text-transform: capitalize;
}

.navigation>li>a:hover {
    color: rgb(242, 255, 0);
    transition: all .3 ease-in-out;
}

.active {
    color: white;
    padding: 15px;
}

.active:hover {
    color: white;
}
h1 {
            color: white;
            text-align: center;
            border: 1px solid black;
            border-radius: 7px;
            padding: 10px;
        }
        
        .hasil {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 600px;
        }
        
        .order-details {
            background-color: white;
            width: 600px;
            padding: 20px;
            border: 5px solid black;
            border-radius: 4px;
        }
        
        .order-details h2 {
            text-align: center;
        }
        
        .order-details pre {
            font-family: Consolas, monospace;
            font-size: 14px;
            white-space: pre-wrap;
            background-color: lightgrey;
            padding: 10px;
        }
        
        .order-details a {
            display: block;
            margin-top: 10px;
            text-align: center;
        }
        footer {
  background-color: #333;
  color: #fff;
  padding: 20px 0;
  width: 100%;
  text-align: center;
}

footer p {
  margin: 0;
  font-size: 14px;
}
    </style>
</head>
<body>
    <header>
            <div class="container">
            <nav class="wrapper">
                <div class="brand">
                    <div class="firstname">TICKET</div>
                    <div class="lastname">thing.</div>
                </div>
                <ul class="navigation">
                    <li><a href="mainpage.php">home</a></li>
                    <li><a href="aboutpage.php" class="active">about me</a></li>
                    <li><a href="contactpage.php">contact me</a></li>    
                    <li><a href="logout.php">log out</a></li>
                </ul>
            </nav>
        </div>
        </header>
        <h1>Rincian Pengiriman</h1>
    <div class="hasil">
        <div class="order-details">
            <h2>Data Pengiriman :</h2>
            <?php
            // Membaca isi file pesanan.txt
            $file = "pesanan.txt";
            $data = file_get_contents($file);
            
            // Menampilkan data pesanan
            if (!empty($data)) {
                echo "<pre>" . $data . "</pre>";
            } else {
                echo "<p>Tidak ada data pesanan yang tersedia.</p>";
            }
            ?>
            <a href="mainpage.php"><b>Kembali Ke Home<b></a>
        </div>
    </div>
    <footer>
  <div class="foot">
    <p>&copy; 2200018364-Muhammad Hifdzul Fatih. 2023 TICKETTHING. All rights reserved.</p>
  </div>
</footer>
</body>
</html>