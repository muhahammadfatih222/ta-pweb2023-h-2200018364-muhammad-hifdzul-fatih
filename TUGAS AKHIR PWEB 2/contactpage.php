<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>TUGAS AKHIR PWEB 2023</title>
    <style>
        * {
    margin: 0;
    padding: 0;
}

body {
    position: relative;
    margin: 0;
    padding: 0;
    height: 100%;
    background-color: rgba(93, 119, 255, 1);
    font-family: "Roboto", sans-serif;
     
}

html {
    scroll-behavior: smooth;
}
.container {
    display: flex;
    width: 100%;
}

.wrapper {
    display: flex;  
    position: fixed;
    justify-content: space-between;
    align-items: center;
    font-size: 20px;    
    padding: 5px;
    width: 100%;
    height: 80px;   
    background-color:black;
    box-shadow: 0 7px 15px 0 rgba(0, 0, 0, 0.5);
}

.brand {
    display: flex;
    flex-direction: row;
    align-items: center;
    font-size: 1.5em;
    padding: 15px;
    text-transform: capitalize;
}

.firstname {
    color: white;
    font-weight: 700;
}

.lastname {
    color: rgb(213, 177, 33);
    font-weight: 400;
    padding-left: 4px;
}

.navigation {
    display: flex;
    justify-content: center;
    align-items: center;
}

.navigation>li {
    list-style-type: none;
    padding: 15px;
}

.navigation > li > a {
    color: white;
    font-size: 20px;
    text-decoration: none; 
    text-transform: capitalize;
}

.navigation>li>a:hover {
    color: rgb(242, 255, 0);
    transition: all .3 ease-in-out;
}

.active {
    color: white;
    padding: 15px;
}

.active:hover {
    color: white;
}
footer {
  background-color: #333;
  color: #fff;
  padding: 20px 0;
  width: 100%;
  text-align: center;
}

footer p {
  margin: 0;
  font-size: 14px;
}
    </style>
</head>
<body>
    <header>
            <div class="container">
            <nav class="wrapper">
                <div class="brand">
                    <div class="firstname">TICKET</div>
                    <div class="lastname">thing.</div>
                </div>
                <ul class="navigation">
                    <li><a href="mainpage.php">home</a></li>
                    <li><a href="aboutpage.php" class="active">about me</a></li>
                    <li><a href="contactpage.php">contact me</a></li>  
                    <li><a href="logout.php">log out</a></li>  
                </ul>
            </nav>
        </div>
        </header>
        <section id="menu3">
            <h4>Contact Me</h4>
            
            <img src="gmail.png" alt="gmail" style="height: 20px; width: 20px;">
            <a href="mailto: muhahammadfatih222@gmail.com" onclick="showAlert()">E-mail</a>

        <script>
        function showAlert() {
        alert("Anda akan menuju ke email saya");
        }
        </script>
            <br><img src="fb.png" alt="fb" style="height: 20px; width: 20px;">
            <a href="https://www.facebook.com/muhammad.fatih.3766" onclick="showAlertt()">Facebook</a>

        <script>
        function showAlertt() {
        alert("Anda akan menuju ke halaman Facebook saya");
        }
        </script>
        <br> <img src="ig.png" alt="ig" style="height: 20px; width: 20px;">
            <a href="https://www.instagram.com/hivvzul/" onclick="showAlerttt()">Instagram</a>

        <script>
        function showAlerttt() {
        alert("Anda akan menuju ke halaman Instagram saya");
        }
        </script>
        </section>
        <footer>
  <div class="foot">
    <p>&copy; 2200018364-Muhammad Hifdzul Fatih. 2023 TICKETTHING. All rights reserved.</p>
  </div>
</footer>
</body>
</html>