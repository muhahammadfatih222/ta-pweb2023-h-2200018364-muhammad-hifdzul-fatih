<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>TUGAS AKHIR PWEB 2023</title>
    <style>
        * {
    margin: 0;
    padding: 0;
}

body {
    position: relative;
    margin: 0;
    padding: 0;
    height: 100%;
    background-color: rgba(93, 119, 255, 1);
    font-family: "Roboto", sans-serif;
     
}

html {
    scroll-behavior: smooth;
}
.container {
    display: flex;
    width: 100%;
}

.wrapper {
    display: flex;  
    position: fixed;
    justify-content: space-between;
    align-items: center;
    font-size: 20px;    
    padding: 5px;
    width: 100%;
    height: 80px;   
    background-color:black;
    box-shadow: 0 7px 15px 0 rgba(0, 0, 0, 0.5);
}

.brand {
    display: flex;
    flex-direction: row;
    align-items: center;
    font-size: 1.5em;
    padding: 15px;
    text-transform: capitalize;
}

.firstname {
    color: white;
    font-weight: 700;
}

.lastname {
    color: rgb(213, 177, 33);
    font-weight: 400;
    padding-left: 4px;
}

.navigation {
    display: flex;
    justify-content: center;
    align-items: center;
}

.navigation>li {
    list-style-type: none;
    padding: 15px;
}

.navigation > li > a {
    color: white;
    font-size: 20px;
    text-decoration: none; 
    text-transform: capitalize;
}

.navigation>li>a:hover {
    color: rgb(242, 255, 0);
    transition: all .3 ease-in-out;
}

.active {
    color: white;
    padding: 15px;
}

.active:hover {
    color: white;
}
h1 {
            padding: 50px;
            margin-bottom: 40px;
        }

        .order-success {
            margin-top: 40px;
        }

        .order-success a {
            text-decoration: none;
            background-color: lightseagreen;
            color: black;
            font-weight: bold;
            padding: 10px 20px;
            border-radius: 4px;
            margin-right: 10px;
            transition: background-color 0.3s;
        }

        .order-success a:hover {
            background-color: gold;
        }
        footer {
  background-color: #333;
  color: #fff;
  padding: 20px 0;
  width: 100%;
  text-align: center;
}

footer p {
  margin: 0;
  font-size: 14px;
}
    </style>
</head>
<body>
    <header>
            <div class="container">
            <nav class="wrapper">
                <div class="brand">
                    <div class="firstname">TICKET</div>
                    <div class="lastname">thing.</div>
                </div>
                <ul class="navigation">
                    <li><a href="mainpage.php">home</a></li>
                    <li><a href="aboutpage.php" class="active">about</a></li>
                    <li><a href="contactpage.php">contact me</a></li>    
                    <li><a href="logout.php">log out</a></li>
                </ul>
            </nav>
        </div>
        </header>
        <h1>Terima Kasih Telah Mengirim Barang di WADUH Express </h1>
    <div class="order-success">
        <a href="index.php">Kirim Lagi</a>
        <a href="lihatpage.php">Lihat Rincian Pengiriman</a>
    </div>

    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Mengambil nilai input dari form
        $name = $_POST["name"];
        $email = $_POST["email"];
        $notelp = $_POST["notelp"];
        $formto = $_POST["formto"];
        $jumlah = $_POST["jumlah"];

        // Menghitung total harga berdasarkan paket dan berat
        $harga = 0;
        if ($formto == "KONSER COLDPLAY") {
            $harga = 250000;
        } elseif ($formto == "KONSER GREENDAY") {
            $harga = 300000;
        } elseif ($formto == "KONSER BLINK-182") {
            $harga = 350000;
        }
        $totalPrice = $harga * $jumlah;

        // Format data yang akan disimpan dalam file .txt
        $data = "Nama:   ". $name . "\n" .
                "E-Mail: " . $email . "\n" .
                "No Telp:  " . $notelp . "\n" .
                "Pesan Tiket: " . $formto . "\n" .
                "Jumlah tiket: " . $jumlah . "\n" .
                "Total Harga: Rp " . number_format($totalPrice, 2) . "\n";


        // Nama file untuk menyimpan data pesanan
        $file = "pesanan.txt";

        // Menambahkan data pesanan ke file .txt
        file_put_contents($file, $data, FILE_APPEND | LOCK_EX);

        }
    ?>
    <footer>
  <div class="foot">
    <p>&copy; 2200018364-Muhammad Hifdzul Fatih. 2023 TICKETTHING. All rights reserved.</p>
  </div>
</footer>
</body>
</html>