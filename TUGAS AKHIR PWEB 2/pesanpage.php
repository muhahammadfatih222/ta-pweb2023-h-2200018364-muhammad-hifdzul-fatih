<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>TUGAS AKHIR PWEB 2023</title>
    <style>
        * {
    margin: 0;
    padding: 0;
}

body {
    position: relative;
    margin: 0;
    padding: 0;
    height: 100%;
    background-color: rgba(93, 119, 255, 1);
    font-family: "Roboto", sans-serif;
     
}

html {
    scroll-behavior: smooth;
}
.container {
    display: flex;
    width: 100%;
}

.wrapper {
    display: flex;  
    position: fixed;
    justify-content: space-between;
    align-items: center;
    font-size: 20px;    
    padding: 5px;
    width: 100%;
    height: 80px;   
    background-color:black;
    box-shadow: 0 7px 15px 0 rgba(0, 0, 0, 0.5);
}

.brand {
    display: flex;
    flex-direction: row;
    align-items: center;
    font-size: 1.5em;
    padding: 15px;
    text-transform: capitalize;
}

.firstname {
    color: white;
    font-weight: 700;
}

.lastname {
    color: rgb(213, 177, 33);
    font-weight: 400;
    padding-left: 4px;
}

.navigation {
    display: flex;
    justify-content: center;
    align-items: center;
}

.navigation>li {
    list-style-type: none;
    padding: 15px;
}

.navigation > li > a {
    color: white;
    font-size: 20px;
    text-decoration: none; 
    text-transform: capitalize;
}

.navigation>li>a:hover {
    color: rgb(242, 255, 0);
    transition: all .3 ease-in-out;
}

.active {
    color: white;
    padding: 15px;
}

.active:hover {
    color: white;
}
.form {
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
        }
        .order-form {
            width: 1000px;
            padding: 150px;
            background-color: rgba(93, 119, 255, 1);
            border-radius: 4px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }
        
        .order-form h2 {
            text-align: center;
            margin-bottom: 20px;
        }
        
        .order-form table {
            width: 100%;
        }
        
        .order-form th, .order-form td {
            padding: 15px;
            border: 1px solid #ccc;
        }
        
        .order-form th {
            background-color: rgb(213, 177, 33);
            color: black;
            text-align: left;
        }
        
        .order-form input[type=text], .order-form select {
            width: 90%;
            padding: 8px;
            border-radius: 4px;
            border: 1px solid #ccc;
        }
        
        .order-form button {
            background-color: rgb(213, 177, 33);
            color: black;
            padding: 10px 20px;
            border: none;/
            border-radius: 4px;
            cursor: pointer;
            float: right;
            margin-top: 20px;
        }
        footer {
  background-color: #333;
  color: #fff;
  padding: 20px 0;
  width: 100%;
  text-align: center;
}

footer p {
  margin: 0;
  font-size: 14px;
}
    </style>
</head>
<body>
    <header>
            <div class="container">
            <nav class="wrapper">
                <div class="brand">
                    <div class="firstname">TICKET</div>
                    <div class="lastname">thing.</div>
                </div>
                <ul class="navigation">
                    <li><a href="mainpage.php">home</a></li>
                    <li><a href="aboutpage.php" class="active">about me</a></li>
                    <li><a href="contactpage.php">contact us</a></li>    
                    <li><a href="logout.php">logout</a></li>    
                </ul>
            </nav>
        </div>
        </header>
        <div class=form>
        <div id="order-form" class="order-form" >
            <h2>Form Pengiriman Barang</h2>
            <form action="prosespesan.php" method="post">
                <table>
                    <tr>
                        <th>Nama :</th>
                        <td><input type="text" name="name" id="name" required></td>
                    </tr>
                    <tr>
                        <th>E-Mail :</th>
                        <td><input type="text" name="email" required></td>
                    </tr>
                    <tr>
                    <tr>
                        <th>No Telp :</th>
                        <td><input type="text" name="notelp" id="notelp" required></td>
                    </tr>
                        <th>Tiket :</th>
                        <td>
                            <select name="formto" id="formto" required>
                                <option value="">Pilih Tiket</option>
                                <option value="KONSER COLDPLAY">KONSER COLDPLAY</option>
                                <option value="KONSER GREENDAY">KONSER GREENDAY</option>
                                <option value="KONSER BLINK-182">KONSER BLINK-182</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Jumlah Pesan:</th>
                        <td><input type="text" name="jumlah" required></td>
                    </tr>
                </table>
                <button type="submit">Kirim</button>
            </form>
        </div>
    </div>
</div>
  <script>function selectPackage(packageName) {
            var orderForm = document.getElementById('order-form');
            var packageSelect = document.getElementById('formto');
            
            orderForm.style.display = 'block';
            packageSelect.value = packageName;
            
        }
        </script>
    <footer>
  <div class="foot">
    <p>&copy; 2200018364-Muhammad Hifdzul Fatih. 2023 TICKETTHING. All rights reserved.</p>
  </div>
</footer>
</body>
</html>