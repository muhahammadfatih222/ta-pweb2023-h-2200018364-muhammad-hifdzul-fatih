
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TUGAS AKHIR PWEB 2023</title>
    <script src="login.js"></script>
    <style>
        * {
    margin: 0;
    padding: 0;
}
        .container {
    display: flex;
    width: 100%;
}

.wrapper {
    display: flex;  
    position: fixed;
    justify-content: space-between;
    align-items: center;
    font-size: 20px;    
    padding: 5px;
    width: 100%;
    height: 80px;   
    background-color: #5D77FF;
    box-shadow: 0 7px 15px 0 rgba(0, 0, 0, 0.5);
}

.brand {
    display: flex;
    flex-direction: row;
    align-items: center;
    font-size: 1.5em;
    padding: 15px;
    text-transform: capitalize;
}

.firstname {
    color: white;
    font-weight: 700;
}

.lastname {
    color: rgb(213, 177, 33);
    font-weight: 400;
    padding-left: 4px;
}
.login {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-color: #f0f8ff;
        }

        form {
            display: inline-block;
            text-align: left;
            border: 1px solid #000;
            border-radius: 7px;
            padding: 20px;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="text"],
        input[type="password"] {
            width: 200px;
            padding: 5px;
            margin-bottom: 10px;
        }

        input[type="button"] {
            padding: 8px 20px;
            background-color: rgb(213, 177, 33);
            color: #fff;
            border: none;
            cursor: pointer;
        }

        input[type="button"]:hover {
            background-color: #4CAF50;
        }
    </style>
</head>
<body>
    <header>
            <div class="container">
            <nav class="wrapper">
                <div class="brand">
                    <div class="firstname">TICKET</div>
                    <div class="lastname">thing.</div>
                </div>
            </nav>
        </div>
        </header>
        <div class="login">
        <form>
            <label for="username">Username:</label>
            <input type="text" id="username" name="username" required><br><br>
    
            <label for="password">Password:</label>
            <input type="password" id="password" name="password" required><br><br>
    
            <input type="button" value="Login" onclick="login()">
        </form>
    </div>
</body>
</html>